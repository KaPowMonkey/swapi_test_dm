# Star Wars Api Subset Test Demo
Testing tools used:
mocha, chai, supertest, nock

Test that can be executed using ci pipeline, are divided into four stages
1. Unit Tests

2. Mocking Test for helper library (SDK)
    nock is used for mocking 
    responsed for mocking are in fixture repository

3. Api Tests
  Testing routes provided by the swapi-swagger.json

4. Integration Tests
  Tests compare responses returned by swapi-node helper and those returned 
  by requesting Star Wars API directly.

