import request from '../config/common';
import { expect } from 'chai';

describe('API People', () => {
    it('GET people/', () => {
        return request.get('people/').then((res) => {
            expect(res.body.results).to.not.be.empty;
        });
    });

    it('GET people/:id', () => {
        return request.get('people/1/').then((res) => {
            expect(res.body.name).to.be.eq("Luke Skywalker");
        });
    });

    it('GET people with query params', () => {
        const url = 'people/?page=2';

        return request.get(url).then((res) => {
            expect(res.body.results[0].name).to.be.eq('Anakin Skywalker');
        });
    });

    it('GET people with search params', () => {
        const url = 'people/?search=luke';

        return request.get(url).then((res) => {
            expect(res.body.results[0].name).to.be.eq('Luke Skywalker');
        });
    });

});

describe('API Films', () => {

    it('GET films/', async () => {
        const res = await request.get('films/');
        expect(res.body.results[0].episode_id).to.be.eq(4);
    });

    it('GET films/:id', async () => {
       const res = await request.get('films/1/').expect(200);
    });
});

describe('API Starships', () => {

    it('GET starships/', async () => {
        const res = await request.get('starships/').expect(200);
        expect(res.body.results[0].length).to.be.eq('150');
    });

});

