import request from '../config/common';
import { expect } from 'chai';

const swapi = require('swapi-node');

describe('GET API vs SDK: People', () => {

    it('GET people/:id ', async () => {        
        const swRes = await swapi.getPerson(1);
        const swName = await swRes.getName();

        const peopleRes = await request.get('people/1/').expect(200);

        expect(peopleRes.body.name).to.be.eq(swName);
        });
});
