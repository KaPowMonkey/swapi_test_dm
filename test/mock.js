import qa from '../config/qa';
import { expect } from 'chai';
const baseUrl = qa.baseUrl;

const swapi = require('swapi-node');
const swapiFilmListMock = require('../fixtures/swapi_mock/film_list.json');
const swapiFilmMock = require('../fixtures/swapi_mock/film_1.json');
const nock = require('nock');

describe('Mock Films', function() {

    it('swapi.GET films has keys', function(done) {
        const url = `${baseUrl}films/`;

        const scope = nock(baseUrl).get("/films/").reply(200, swapiFilmListMock);

        swapi.get(url).then(function(result){
            expect(result).to.include.all.keys('nextPage', 'previousPage', 'getCount', 'getNext', 'getPrevious');            
            done();
        }).catch(error => {
            done(new Error ("Test GET Films FAILED"));
        });

    });

    it('swapi.GET film:id include keys', function(done) {
        const url = `${baseUrl}films/1/`;

        const scope = nock(baseUrl).get("/films/1/").reply(200, swapiFilmMock);

        swapi.get(url).then(function(result){
            expect(result).to.include.all.keys('nextPage', 'getTitle', 'getDirector', 'getCharacters', 'getUrl');            
            done();
        }).catch(error => {
            done(new Error ("Test swapi.GET Film:id FAILED"));
        });

    });


});